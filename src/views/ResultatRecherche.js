import React, {Component} from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Slide from '../components/component/slide/Slide';
import IosLeafOutline from 'react-ionicons/lib/IosLeafOutline';
import Link from '@material-ui/core/Link';

class Accueil extends Component{
    componentDidMount() {
        document.title = "Résultats de la recherche";
    }

    render(){
        return (
            <React.Fragment>
                <CssBaseline />
                <Container maxWidth="md">
                <h3 style={{color:'green'}}>Bienvenue sur le site APM</h3>
                <Typography paragraph align="justify"  style={{color:'black'}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                ut labore et dolore magna aliqua. Rhoncus dolor purus non enim praesent elementum
                facilisis leo vel. Risus at ultrices mi tempus imperdiet. Semper risus in hendrerit
                gravida rutrum quisque non tellus. Convallis convallis tellus id interdum velit laoreet id
                donec ultrices. Odio morbi quis commodo odio aenean sed adipiscing. Amet nisl suscipit
                adipiscing bibendum est ultricies integer quis. Cursus euismod quis viverra nibh cras.
                Metus vulputate eu scelerisque felis imperdiet proin fermentum leo. Mauris commodo quis
                imperdiet massa tincidunt. Cras tincidunt lobortis feugiat vivamus at augue. At augue eget
                arcu dictum varius duis at consectetur lorem. Velit sed ullamcorper morbi tincidunt. Lorem
                donec massa sapien faucibus et molestie ac.
                </Typography>
                <h6 style={{color:'green'}}>Direction les aires protégées</h6>
                <Link href="/aireprotegee">
                    <IosLeafOutline fontSize="60px" color="#43853d" shake="true" />
                </Link>
            </Container>
            </React.Fragment>
        );
    }
}

export default Accueil;