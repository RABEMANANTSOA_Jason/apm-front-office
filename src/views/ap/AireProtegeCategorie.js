import React, {Component} from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import AireProtegeeCategorieStepper from '../../components/component/stepper/AireProtegeeCategorieStepper';
import ModeGestionListe from '../../components/component/liste/ModeGestionListe';

import API from '../../services/API';
class AireProtegeCategorie extends Component{
    constructor (props) {
        super(props);
        this.state = {
            categories: [],
            mode_gestion : []
        };
    }
    componentDidMount() {
        document.title = "Catégories des aires protégées";
        API.get(`Categorie_iucn`).then(res => {
            const Allcategories = res.data;
            this.setState({ categories:Allcategories });
        });
        API.get(`mode_gestion`).then((res) => {
            const All = res.data;
            this.setState({ mode_gestion: All });
        });
    }

    render(){
        return (
            <React.Fragment>
                <CssBaseline />
                <Container maxWidth="md">
                <h4 style={{color:'black'}}>Mode de gestion</h4>
                <ModeGestionListe data={this.state.mode_gestion}/>
                <br/>
                <h4 style={{color:'black'}}>Catégories des aires protégées</h4>
                <AireProtegeeCategorieStepper categories={this.state.categories}/>
                </Container>
            </React.Fragment>
        );
    }
}

export default AireProtegeCategorie;