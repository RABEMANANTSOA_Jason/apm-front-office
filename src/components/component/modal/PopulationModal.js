import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal'
import MdHelpCircle from 'react-ionicons/lib/MdHelpCircle';
import Image from 'react-bootstrap/Image';
import { Typography } from '@material-ui/core';
import imageURL from '../../../services/imageURL';

export default function PopulationModal({titre,image,description,impacts}){
    const [show, setShow] = useState(false);

    return (
        <>
        <MdHelpCircle fontSize="20px" color="blue" beat={true} onClick={() => setShow(true)}/>

        <Modal show={show} onHide={() => setShow(false)} dialogClassName="modal-90w" 
        aria-labelledby="example-custom-modal-styling-title" size="xl">

            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title" align="center">{titre}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Image className="d-block w-100" height="auto" src={imageURL+image} alt="" />
            <b>Description</b>
            <Typography align="justify" dangerouslySetInnerHTML={{__html: description}}/>
            <br/>
            <b>Impacts</b>
            <Typography align="justify" dangerouslySetInnerHTML={{__html: impacts}}/>
            </Modal.Body>

        </Modal>
        </>
    );
}