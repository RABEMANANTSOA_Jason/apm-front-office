import React, {Component} from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';

class NotFound extends Component{
    componentDidMount() {
        document.title = "404 Page non trouvée";
    }

    render(){
        return (
            <React.Fragment>
                <CssBaseline />
                <Container maxWidth="md">
                <h3 style={{color:'black'}}>Erreur 404 page non trouvée</h3>
                
                </Container>
            </React.Fragment>
        );
    }
}

export default NotFound;