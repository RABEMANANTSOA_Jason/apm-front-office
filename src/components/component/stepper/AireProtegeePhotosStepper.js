import React,{useState} from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import Modal from 'react-bootstrap/Modal';
import Image from 'react-bootstrap/Image';
import imageURL from '../../../services/imageURL';
const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 400,
    flexGrow: 1,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    height: 50,
    paddingLeft: theme.spacing(4),
    backgroundColor: 'green',
    color: 'white'
  },
  img: {
    height: 255,
    display: 'block',
    maxWidth: 400,
    overflow: 'hidden',
    width: '100%',
  },
}));

export default function AireProtegeePhotosStepper({data}) {
  const classes = useStyles();
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = data.length;
  const [show, setShow] = useState(false);
  const [image, setImage] = useState("");

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    (isresultat(data)? (
      <div className={classes.root}>
      <Paper square elevation={0} className={classes.header}>
        <Typography>{data[activeStep].description_image}</Typography>
      </Paper>
      <AutoPlaySwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
      >
        {data.map((step, index) => (
          <div key={step.label}>
            {Math.abs(activeStep - index) <= 2 ? (
              <img className={classes.img} src={imageURL+step.image} alt={step.label} onClick={() => {setShow(true);setImage(step.imgPath);}} />
            ) : null}
          </div>
        ))}
      </AutoPlaySwipeableViews>
      <MobileStepper
      variant="progress"
      steps={maxSteps}
      position="static"
      activeStep={activeStep}
      className={classes.root}
      />
      <Modal show={show} onHide={() => setShow(false)} dialogClassName="modal-90w" 
        aria-labelledby="example-custom-modal-styling-title" size="xl">
            <Modal.Body>
            <Image className="d-block w-100" height="auto" src={image} alt="" />
            </Modal.Body>

        </Modal>
    </div>
    ):(<p>Aucune photo</p>))
  );
}

function isresultat(data){
  if(data.length > 0){ return true;}
  return false;
}