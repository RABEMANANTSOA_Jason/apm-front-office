import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: '#76ED79',
  },
  inline: {
    display: 'inline',
  },
}));

export default function ModeGestionListe({data}) {
  const classes = useStyles();

  return (
    <React.Fragment>
    <List className={classes.root}>
    {data.map((row,index) => (
        <ListItem alignItems="flex-start" key={index}>
        <ListItemAvatar>
          <Avatar style={{backgroundColor:'blue'}}>{index+1}</Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={row.type_mode_gestion}
        />
      </ListItem>
    ))}
    </List>
    </React.Fragment>
  );
}
