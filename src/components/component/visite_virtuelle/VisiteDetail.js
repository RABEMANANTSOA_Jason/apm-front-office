import React from 'react';
import 'aframe';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import Radio from '@material-ui/core/Radio';
import Tooltip from '@material-ui/core/Tooltip';
import imageURL from '../../../services/imageURL';

const GreenRadio = withStyles({
  root: {
    color: green[400],
    '&$checked': {
      color: green[600],
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);
const useStyles = makeStyles((theme) => ({
    div: {
        width: 'auto',
        height: '600px'
    }
  }));
export default function VisiteDetail({visite}) {
  const classes = useStyles();
  const [selectedValue, setSelectedValue] = React.useState('centre');
  const [image, setImage] =React.useState("#centre");
  const handleChange = (event) => {
    setSelectedValue(event.target.value);
    setImage(event.target.value);
  };
  return (
    <React.Fragment>
    <div>
      <h3>{visite.nom_visite}</h3>
    <Tooltip title="Nord">
      <GreenRadio
        checked={selectedValue === '#nord'}
        onChange={handleChange}
        value="#nord"
        name="radio-button-demo"
      />
    </Tooltip>
    <Tooltip title="Ouest">
      <GreenRadio
        checked={selectedValue === '#ouest'}
        onChange={handleChange}
        value="#ouest"
        name="radio-button-demo"
      />
    </Tooltip>
    <Tooltip title="Centre">
      <GreenRadio
        checked={selectedValue === '#centre'}
        onChange={handleChange}
        value="#centre"
        name="radio-button-demo"
      />
    </Tooltip>
    <Tooltip title="Est">
      <GreenRadio
        checked={selectedValue === '#est'}
        onChange={handleChange}
        value="#est"
        name="radio-button-demo"
      />
    </Tooltip>
    <Tooltip title="Sud">
      <GreenRadio
        checked={selectedValue === '#sud'}
        onChange={handleChange}
        value="#sud"
        name="radio-button-demo"
      />
    </Tooltip>
    </div>
      <div className={classes.div}>

        <a-scene renderer="antialias: true;" embedded >
            <a-assets>
            <img id="nord" crossOrigin="anonymous" src={imageURL+visite.image_nord} alt="nord"/>
            <img id="ouest" crossOrigin="anonymous" src={imageURL+visite.image_ouest} alt="ouest"/>
            <img id="centre" crossOrigin="anonymous" src={imageURL+visite.image_centre} alt="centre"/>
            <img id="est" crossOrigin="anonymous" src={imageURL+visite.image_est} alt="est"/>
            <img id="sud" crossOrigin="anonymous" src={imageURL+visite.image_sud} alt="sud"/>
            </a-assets>
        <a-sky src={image}/>
        </a-scene>
      </div>
      </React.Fragment>
  );
}
