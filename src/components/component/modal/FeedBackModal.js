import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import IosSend from "react-ionicons/lib/IosSend";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import { Button } from "@material-ui/core";
import API from "../../../services/API";

const useStyles = makeStyles((theme) => ({
  root: {},
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function FeedBackModal() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [nom, setNom] = React.useState("");
  const [prenoms, setPrenoms] = React.useState("");
  const [mail, setMail] = React.useState("");
  const [sujet, setSujet] = React.useState("");
  const [message, setMessage] = React.useState("");
  const [error, setError] = new React.useState("");
  const handleChangeNom = (event) => {
    setNom(event.target.value);
  };

  const handleChangePrenoms = (event) => {
    setPrenoms(event.target.value);
  };

  const handleChangeMail = (event) => {
    setMail(event.target.value);
  };

  const handleChangeSujet = (event) => {
    setSujet(event.target.value);
  };

  const handleChangeMessage = (event) => {
    setMessage(event.target.value);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const sendMessage = () => {
    let data = {
        nom : nom,
        prenoms : prenoms,
        mail : mail,
        sujet : sujet,
        message : message
    }
    console.log(data);
    API.post('Feedback/create',data).then(res => {
        handleClose();
    }).catch(error => { setError(error.message)})
  }

  return (
    <div>
      <IosSend fontSize="30px" color="blue" beat={true} onClick={handleOpen} />
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">Envoyer un message</h2>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      id="filled-multiline-flexible"
                      label="Nom"
                      fullWidth
                      value={nom}
                      onChange={handleChangeNom}
                      variant="filled"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      id="filled-multiline-flexible"
                      label="Prénoms"
                      fullWidth
                      value={prenoms}
                      onChange={handleChangePrenoms}
                      variant="filled"
                    />
                  </Grid>
                  <br />
                  <Grid item xs={12} sm={12}>
                    <TextField
                      id="standard-multiline-flexible"
                      label="Email"
                      fullWidth
                      value={mail}
                      onChange={handleChangeMail}
                    />
                  </Grid>
                  <br />
                  <Grid item xs={12} sm={12}>
                    <TextField
                      id="standard-multiline-flexible"
                      label="Sujet"
                      fullWidth
                      value={sujet}
                      onChange={handleChangeSujet}
                    />
                  </Grid>
                  <br />
                  <br />
                  <br />
                  <Grid item xs={12} sm={12}>
                    <TextField
                      id="outlined-multiline-static"
                      label="Message"
                      multiline
                      fullWidth
                      rows={4}
                      value={message}
                      variant="outlined"
                      onChange={handleChangeMessage}
                    />
                  </Grid>
                  <Grid item xs={12} sm={5}/>
                  <Grid item xs={12} sm={2}>
                  <p style={{color:'red'}}>{error}</p>
                  <Button onClick={sendMessage}>Enoyver</Button>
                    </Grid>
                  <Grid item xs={12} sm={5}/>
                </Grid>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
