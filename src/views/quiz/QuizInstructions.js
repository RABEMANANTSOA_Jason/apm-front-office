import React,{Fragment} from 'react';
import Link from '@material-ui/core/Link';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";

import answer from '../../assets/img/answer.png';
import fiftyFifty from '../../assets/img/fiftyFifty.png';
import hints from '../../assets/img/hints.png';
import options from '../../assets/img/options.PNG';
import '@mdi/font/css/materialdesignicons.min.css';
import 'materialize-css/dist/js/materialize.min.js';
import '../../styles/styles.scss';

import IosHomeOutline from 'react-ionicons/lib/IosHomeOutline';
import IosGameControllerB from 'react-ionicons/lib/IosGameControllerB';
class QuizInstructions extends React.Component{
    componentDidMount(){
        document.title = "Instructions quiz";
        console.log(this.props.match.params.id);
    }
    render(){
        return(
            <Fragment>
                <CssBaseline/>
                <div className="instructions container">
                <h3 style={{ color: "green" }}>Règles du jeu</h3>
                    <ul className="browser-default" id="main-list">
                        <Typography>
                            Le jeu dure <b style={{ color: "red" }}>15 min</b> et se termine une fois le temps écoulé
                            <p>Chaque question est composée de 4 options</p>
                        </Typography>
                        <img src={options} alt="Quiz App options example" />
                        <Typography>
                            <br/>
                            Sélectionner une option à chaque question
                        </Typography>
                        <img src={answer} alt="Quiz App answer example" />
                        <Typography>
                            <br/>
                            Chaque partie possède 2 types d'aides :
                            <p><b style={{ color: "red" }}>5</b> indices</p>
                            <p><b style={{ color: "red" }}>2</b> moitié-moitié</p>
                        </Typography>
                        <Typography>
                            Utiliser un indice en cliquant sur l'icône
                            <span className="mdi mdi-lightbulb-on mdi-24px lifeline-icon"></span>
                            supprimera <b style={{ color: "red" }}>1</b> mauvaise réponse et laissera <b style={{ color: "red" }}>2</b> mauvaises réponses et la <b style={{ color: "green" }}>bonne réponse</b>. Vous pouvez utiliser autant d'indice qu'il y en a sur une question.
                        </Typography>
                        <img src={hints} alt="Quiz App hints example" />
                        <Typography>
                            <br/>
                            Utiliser une moitié-moitié en cliquant sur l'icône
                            <span className="mdi mdi-set-center mdi-24px lifeline-icon"></span>
                            supprimera <b style={{ color: "red" }}>2</b> mauvaises réponses, et laissera <b style={{ color: "red" }}>1</b> mauvaise réponse et la <b style={{ color: "green" }}>bonne réponse</b>
                        </Typography>
                        <img src={fiftyFifty} alt="Quiz App Fifty-Fifty example"/>

                        <Typography>
                            Vous êtes libre de quitter le jeu à n'importe quel moment
                            Votre score sera alors révélé
                        </Typography>
                        <Typography>
                            Le chronomètre commencera dès que le quiz apparaîtra
                        </Typography>
                        <Typography>
                            Aller, c'est parti :) 
                        </Typography>
                    </ul>
                    <div>
                    <Grid container spacing={0}>
                        <Grid item xs={12} sm={2}>
                            <h6 style={{ color: "red" }}>Retour</h6>
                            <Link href="/quiz">
                                <IosHomeOutline fontSize="60px" color="red" beat={true} />
                            </Link>
                        </Grid>
                        <Grid item xs={12} sm={8} />
                        <Grid item xs={12} sm={2} >
                            <h6 style={{ color: "green" }}>Commencer</h6>
                            <Link href={"/quiz/jeu/"+this.props.match.params.id}>
                                <IosGameControllerB fontSize="60px" color="green" beat={true} />
                            </Link>
                        </Grid>
                    </Grid>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default QuizInstructions;