import React, { Component } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Link from "@material-ui/core/Link";
import "@mdi/font/css/materialdesignicons.min.css";
import "../../styles/styles.scss";
import API from "../../services/API";
class Quiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quiz: [],
    };
  }

  componentDidMount() {
    document.title = "Jeux";
    API.get(`quiz`).then((res) => {
      const All = res.data;
      this.setState({ quiz: All });
      console.log(this.state.quiz);
    });
  }
  
  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="md">
          <div id="home">
            <section>
              <div style={{ textAlign: "center" }}>
                <span className="mdi mdi-cube-outline cube mdi-spin"></span>
              </div>
              <h1>Jeux</h1>
              {this.state.quiz.map((row) => (
                <div className="play-button-container">
                  <ul>
                    <li>
                      <Link className="play-button" href={"/quiz/instructions/" + row.id_quiz}>
                        {row.nom_quiz}
                      </Link>
                    </li>
                  </ul>
                </div>
              ))}
            </section>
          </div>
        </Container>
      </React.Fragment>
    );
  }
}

export default Quiz;
