import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';

export default function NavBar() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Navbar bg="info" expand="xl" sticky="top" variant="dark">
    <Navbar.Brand href="/"><img
        alt=""
        src="/icone/green.png"
        width="30"
        height="30"
        className="d-inline-block align-top"
      />{' '}
      APM</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <Nav.Link href="/" style={{color:'white'}}>Accueil</Nav.Link>
        <Nav.Link style={{color:'white'}} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>Aires protégées</Nav.Link>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose}><Nav.Link href="/aireprotegee">Définition</Nav.Link></MenuItem>
          <MenuItem onClick={handleClose}><Nav.Link href="/aireprotegee/categories">Catégories</Nav.Link></MenuItem>
          <Divider/>
          <MenuItem onClick={handleClose}><Nav.Link href="/aireprotegee/liste">Liste</Nav.Link></MenuItem>
        </Menu>
        <Nav.Link href="/gestionnaire" style={{color:'white'}}>Gestionnaires</Nav.Link>
        <Nav.Link href="/cartographie" style={{color:'white'}}>Cartographie</Nav.Link>
        <Nav.Link href="/visite_virtuelle" style={{color:'white'}}>Visite virtuelle</Nav.Link>
        <Nav.Link href="/quiz" style={{color:'white'}}>Quiz</Nav.Link>
        <Nav.Link href="/actualites" style={{color:'white'}}>Actualités</Nav.Link>
        <Nav.Link href="#home" style={{color:'white'}}>Liens utiles</Nav.Link>
      </Nav>
      <Form inline>
        <Nav.Link href="/aireprotegee/liste">
          <Button>Voir les aires protégées</Button>
        </Nav.Link>
      </Form>
    </Navbar.Collapse>
    </Navbar>
  );
}
