import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal'
import { Typography } from '@material-ui/core';
import Link from '@material-ui/core/Link';

export default function CategorieModal({code,statut_fr,categorie,description}){
    const [show, setShow] = useState(false);

    return (
        <>
        <Link href="#" onClick={() => setShow(true)}>{categorie}-{statut_fr}({code})</Link>

        <Modal show={show} onHide={() => setShow(false)} dialogClassName="modal-90w" 
        aria-labelledby="example-custom-modal-styling-title" size="xl">

            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title" align="center">{statut_fr+"("+code+")"}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <b>Statut</b> 
            <Typography>
            {statut_fr+"("+code+")"}
            </Typography>
            <b>Catégorie</b> 
            <Typography>
            {categorie}
            </Typography>
            <b>Description</b>
            <Typography align="justify" dangerouslySetInnerHTML={{__html: description}}/>
            </Modal.Body>

        </Modal>
        </>
    );
}