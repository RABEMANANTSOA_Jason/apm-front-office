import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal'
import { Typography } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import MdHelpCircle from 'react-ionicons/lib/MdHelpCircle';

export default function GestionnaireModal({titre,description,date,tableau}){
    const [show, setShow] = useState(false);

    return (
        <>
        {tableau?(
          <MdHelpCircle fontSize="20px" color="blue" beat={true} onClick={() => setShow(true)}/>
        ):(
          <Link href="#" onClick={() => setShow(true)}>{titre}</Link>
        )}

        <Modal show={show} onHide={() => setShow(false)} dialogClassName="modal-90w" 
        aria-labelledby="example-custom-modal-styling-title" size="xl">

            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title" align="center">{titre}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <b>Date de création</b> 
            <Typography>
            {date}
            </Typography>
            <b>Description</b>
            <Typography align="justify" dangerouslySetInnerHTML={{__html: description}}/>
            </Modal.Body>

        </Modal>
        </>
    );
}