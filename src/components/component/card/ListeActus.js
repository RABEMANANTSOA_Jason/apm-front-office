import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ActuModal from '../modal/ActuModal';
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      maxWidth: 'auto',
    },
    demo: {
      backgroundColor: theme.palette.background.paper,
    },
    title: {
      margin: theme.spacing(4, 0, 2),
    },
  }));

export default function ListeActus({actualites}) {
  const classes = useStyles();
  const dense = false;
  return (
    <div className={classes.root}>
          <div className={classes.demo}>      
            <List dense={dense}>
            {actualites.map((row) => (
                <ListItem key={row.id}>
                    <ActuModal detail={row}/>
                </ListItem>
            ))}
            </List>
          </div>
    </div>
  );
}
