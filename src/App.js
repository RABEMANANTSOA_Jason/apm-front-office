import React from "react";
import "./App.css";
import NavBar from "./components/header/navbar/NavBar";
import "bootstrap/dist/css/bootstrap.min.css";
import Routes from "./components/routes/Routes";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import MenuSpeedDial from "./components/component/speedDial/MenuSpeedDial";
const Background =
  "https://cdn.pixabay.com/photo/2020/03/10/13/24/lemur-catta-4919030_1280.jpg";
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundImage: "url(" + Background + ")",
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    height: "max",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: "theme.palette.text.secondary",
    backgroundColor: "#E1FDB0",
    height: "max",
  },
}));

function App() {
  const classes = useStyles();

  return (
    <div className="App">
      <NavBar />
      <div className={classes.root}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={2} />
          <Grid item xs={12} sm={8}>
            <Paper className={classes.paper}>
              <Routes /> <MenuSpeedDial />
            </Paper>
          </Grid>
          <Grid item xs={12} sm={2} />
        </Grid>
      </div>
    </div>
  );
}

export default App;
