import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal'
import IosPaw from'react-ionicons/lib/IosPaw';
import Image from 'react-bootstrap/Image';
import { Typography } from '@material-ui/core';
import faune from '../../../assets/images/faune.jpg';

export default function FauneModal({icone}){
    const [show, setShow] = useState(false);

    return (
        <>
        <IosPaw fontSize="30px" color="#955503" beat={true} onClick={() => setShow(true)}/>

        <Modal show={show} onHide={() => setShow(false)} dialogClassName="modal-90w" 
        aria-labelledby="example-custom-modal-styling-title" size="xl">

            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title">Faune</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Image className="d-block w-100" height="auto" src={faune} alt="" />
            <Typography>

            </Typography>
            </Modal.Body>

        </Modal>
        </>
    );
}