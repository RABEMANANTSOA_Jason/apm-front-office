import React, {Component} from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import AireProtegeeTimeLine from '../../components/component/timeline/AireProtegeeTimeLine';
import Link from '@material-ui/core/Link';
import MdArrowRoundForward from 'react-ionicons/lib/MdArrowRoundForward';
import MdArrowRoundBack from 'react-ionicons/lib/MdArrowRoundBack';
class AireProtege extends Component{
    componentDidMount() {
        document.title = "Aires protégées";
    }

    render(){
        return (
            <React.Fragment>
                <CssBaseline />
                <Container maxWidth="md">
                <h3 style={{color:'black'}}>Les Aires protégées de Madagascar</h3>
                <h4>Définition</h4>
                <Typography align="justify">
                    Une aire protégée est définie comme: «un espace géographique clairement défini, reconnu, consacré et géré, par tout moyen efficace, juridique ou autre, afin d’assurer à long terme la conservation de la nature ainsi que les services écosystémiques et les valeurs culturelles qui lui sont associées» (Dudley, 2008). Les aires protégées forestières aident à conserver les écosystèmes qui fournissent un habitat, un abri, des aliments, des matières premières, du matériel génétique, une barrière contre les catastrophes naturelles, une source stable de ressources et de nombreux autres biens et services propres à l’écosystème. Elles jouent dès lors un rôle important en aidant les espèces, les populations et les pays à s’adapter aux changements climatiques. En vertu de leur fonction de protection, ces forêts devraient rester exemptes de toute intervention humaine destructive. Elles pourront ainsi continuer à servir de réservoir naturel de biens et de services pour l’avenir.
                </Typography>
                <br/>
                <h4>Rôles</h4>
                <AireProtegeeTimeLine/>                
                <h6 style={{color:'green'}}>Mode de gestion et catégories</h6>
                <h6 style={{color:'green'}}>des aires protégées</h6>
                <Link href="/accueil" align="left">
                    <MdArrowRoundBack fontSize="60px" color="red" beat={true} />
                </Link>
                <Link href="/aireprotegee/categories" align="left">
                    <MdArrowRoundForward fontSize="60px" color="#43853d" beat={true} />
                </Link>
                </Container>
            </React.Fragment>
        );
    }
}

export default AireProtege;