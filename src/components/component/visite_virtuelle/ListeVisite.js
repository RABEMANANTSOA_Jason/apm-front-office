import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import MdGlasses from 'react-ionicons/lib/MdGlasses';
import imageURL from '../../../services/imageURL';
const useStyles = makeStyles({
    root: {
      maxWidth: 345,
    }
  });

export default function Resultats({visites}) {
    const liste_visites = visites;
  return (
    <React.Fragment>
    {isresultat(liste_visites)? (
        <Grid container spacing={3}>
        {liste_visites.map((row) => (
            <Grid item xs={6} key={row.id_ap}>
                {card(row.id,row.nom_visite,row.image_centre)}
            </Grid>
        ))}
    </Grid>
    ) : (
        <h3>Aucun résultats</h3>
    )}
       
    </React.Fragment>
  );
}

function isresultat(visites){
    if(visites.length > 0){ return true;}
    return false;
}

function card(id,titre_visite,image){
    console.log("admin.localhost"+image);
    return(
        <Card className={useStyles.root}>
        <CardActionArea>
            <CardMedia
            component="img"
            alt=""
            height="140"
            image={imageURL+image}
            title={titre_visite}
            />
            <CardContent>
                <Typography gutterBottom variant="h6" component="h4">
                    {titre_visite}
                </Typography>
            </CardContent>
        </CardActionArea>
        <CardActions>
        <Link href={"/visite_virtuelle/detail/"+id}>
            <MdGlasses fontSize="40px" color="black" beat={true}/>
        </Link>
        </CardActions>
        </Card>
    );
}