import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal'
import IosPaperOutline from 'react-ionicons/lib/IosPaperOutline';
import Image from 'react-bootstrap/Image';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import imageURL from '../../../services/imageURL';
const useStyles = makeStyles((theme) => ({
    root1: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: '100%',
    },
    paper: {
        padding: theme.spacing(2),
        color: theme.palette.text.secondary,
      },
    iconButton: {
      padding: 10,
    },
  }));
export default function ActualitesModal({detail}){
    const [show, setShow] = useState(false);
    const classes = useStyles();

    return (
        <>
        <Paper className={classes.root1}>
        <IconButton className={classes.iconButton} >
              <IosPaperOutline fontSize="20px" color="blue" beat={true} onClick={() => setShow(true)}/>
              </IconButton>
              <Typography alignItems="center" noWrap="">{detail.titre_actualite}</Typography>
        </Paper>

        <Modal show={show} onHide={() => setShow(false)} dialogClassName="modal-90w" 
        aria-labelledby="example-custom-modal-styling-title" size="xl">

            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title" align="center">{detail.titre_actualite}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Image className="d-block w-100" height="auto" src={imageURL+detail.image_actualite} alt="" />
            <Typography align="justify">
                {detail.contenu_actualite}
            </Typography>
            </Modal.Body>

        </Modal>
        </>
    );
}