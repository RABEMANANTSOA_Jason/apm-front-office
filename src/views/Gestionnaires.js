import React, { Component } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import API from "../services/API";
import GestionnaireTable from "../components/component/tables/GestionnaireTable";

class Gestionnaire extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gestionnaires: [],
    };
  }

  componentDidMount() {
    document.title = "Gestionnaires";
    API.get(`gestionnaires`).then((res) => {
      const All = res.data;
      this.setState({ gestionnaires: All });
    });
  }

  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="md">
          <h3 style={{ color: "green" }}>Gestionnaire</h3>
          <Typography align="justify">
            Les institutions nationales responsables de la gestion de la
            biodiversité et des aires protégées jouent un rôle très important
            dans l’atténuation des impacts de ces menaces sur les espèces et les
            écosystèmes à travers diverses actions, avec des résultats
            encourageants. Cependant, il reste beaucoup d’efforts à fournir, et
            ces institutions ne possèdent pas toujours toutes les capacités
            requises pour mettre en œuvre les programmes adéquats qui pourraient
            inverser les tendances actuelles ; ou pour susciter l’adhésion des
            acteurs, notamment riverains, à de nouvelles approches de gestion
            des aires protégées.
          </Typography>
          <GestionnaireTable data={this.state.gestionnaires} />
        </Container>
      </React.Fragment>
    );
  }
}

export default Gestionnaire;
