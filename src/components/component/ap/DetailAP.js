import React from "react";
import Image from "react-bootstrap/Image";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import MdInformationCircle from "react-ionicons/lib/MdInformationCircle";
import IosPeople from "react-ionicons/lib/IosPeople";
import MdBookmarks from "react-ionicons/lib/MdBookmarks";
import MdConstruct from "react-ionicons/lib/MdConstruct";
import Avatar from "@material-ui/core/Avatar";
import imageURL from "../../../services/imageURL";
import GestionnaireModal from '../modal/GestionnaireModal';
import CategorieModal from '../modal/CategorieModal';
export default function DetailAP({ data }) {
  return (
    <>
      <h3 style={{ color: "green" }}>{data.nom_du_site}</h3>
      <Image
        className="d-block w-100"
        height="auto"
        src={imageURL + data.ap_image}
        alt=""
      />
      <br />
      <Grid container spacing={0}>
        <Grid item xs={6}>
          <List>
            <ListItem>
              <ListItemAvatar>
                <Avatar style={{ backgroundColor: "white" }}>
                  <MdInformationCircle fontSize="50px" color="#43853d" />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Descriptif" secondary={data.type_ap} />
            </ListItem>
            <Divider variant="inset" component="li" />
            <ListItem>
              <ListItemAvatar>
                <Avatar style={{ backgroundColor: "white" }}>
                  <MdBookmarks fontSize="50px" color="#43853d" />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="Catégorie IUCN"
                secondary={
                  <CategorieModal code={data.code_statut_categorie_iucn} statut_fr={data.statut_fr} categorie={data.categorie} description={data.description_categorie_iucn}/>
                }
              />
            </ListItem>
          </List>
        </Grid>
        <Grid item xs={6}>
          <List>
            <ListItem>
              <ListItemAvatar>
                <Avatar style={{ backgroundColor: "white" }}>
                  <IosPeople fontSize="50px" color="#43853d" />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="Gestionnaire"
                secondary={<GestionnaireModal titre={data.nom_gestionnaire} description={data.description_gestionnaire} date={data.date_creation_gestionnaire}/>}
              />
            </ListItem>
            <Divider variant="inset" component="li" />
            <ListItem>
              <ListItemAvatar>
                <Avatar style={{ backgroundColor: "white" }}>
                  <MdConstruct fontSize="50px" color="#43853d" />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Mode de gestion" secondary="Aucun" />
            </ListItem>
          </List>
        </Grid>
      </Grid>
      <h4 align="left" style={{ color: "black" }}>
        Historique
      </h4>
      <Typography align="justify" dangerouslySetInnerHTML={{__html: data.historique}}/>
    </>
  );
}
