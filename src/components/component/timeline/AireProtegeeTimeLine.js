import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import ClimatModal from '../modal/ClimatModal';

import FloreModal from '../modal/FloreModal';

import FauneModal from '../modal/FauneModal';

import SocialModal from '../modal/SocialModal';

import EconomieModal from '../modal/EconomieModal';


const useStyles = makeStyles((theme) => ({
  paper: {
    padding: '6px 16px',
  },
  secondaryTail: {
    backgroundColor: theme.palette.secondary.main,
  },
  sun:{
    backgroundColor:"#87F18D"
  }
}));

export default function AireProtegeeTimeLine() {
  const classes = useStyles();

  return (
    <Timeline align="alternate">
      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot className={classes.sun}>
            <ClimatModal/>
          </TimelineDot>
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent align="left">
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="h6" component="h1">
              Climat
            </Typography>
            <Typography>Aide dans la lutte contre le réchauffement climatique</Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>
      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot className={classes.sun}>
            <FloreModal/>
          </TimelineDot>
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="h6" component="h1">
              Flore
            </Typography>
            <Typography>Possède une grande diversité d'espèces floriques</Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>
      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot className={classes.sun}>
            <FauneModal/>
          </TimelineDot>
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent align="left">
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="h6" component="h1">
              Faune
            </Typography>
            <Typography>Possède une grande diversité d'espèces fauniques</Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>
      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot className={classes.sun}>
            <SocialModal/>
          </TimelineDot>
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="h6" component="h1">
              Social
            </Typography>
            <Typography>Subvient aux besoins de la population</Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>
      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot className={classes.sun}>
            <EconomieModal/>
          </TimelineDot>
        </TimelineSeparator>
        <TimelineContent align="left">
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="h6" component="h1">
              Economie
            </Typography>
            <Typography>Contribue à l'économie du pays</Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>
    </Timeline>
  );
}
