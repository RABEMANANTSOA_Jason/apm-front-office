import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';

class QuizResultats extends Component {
    constructor (props) {
        super(props);
        this.state = {
            score: 0,
            numberOfQuestions: 0,
            numberOfAnsweredQuestions: 0,
            correctAnswers: 0,
            wrongAnswers: 0,
            hintsUsed: 0,
            fiftyFiftyUsed: 0,
            niveau : 0,
        };
    }

    componentDidMount () {
        const { state } = this.props.location;
        if (state) {
            this.setState({
                score: (state.score / state.numberOfQuestions) * 100,
                numberOfQuestions: state.numberOfQuestions,
                numberOfAnsweredQuestions: state.numberOfAnsweredQuestions,
                correctAnswers: state.correctAnswers,
                wrongAnswers: state.wrongAnswers,
                hintsUsed: state.hintsUsed,
                fiftyFiftyUsed: state.fiftyFiftyUsed,
                niveau : state.niveau
            });
        }
    }

    render () {
        const { state } = this.props.location;
        let stats, remark;
        const userScore = this.state.score;

        if (userScore <= 30 ) {
            remark = 'Tu as encore beaucoup à explorer! Courage!';
        } else if (userScore > 30 && userScore <= 50) {
            remark = 'Meilleur chance la prochaine fois!';
        } else if (userScore <= 70 && userScore > 50) {
            remark = 'Tu peux mieux faire!';
        } else if (userScore >= 71 && userScore <= 84) {
            remark = 'Très bon score!';
        } else {
            remark = 'Tu es un génie! Bravo!';
        }

        if (state !== undefined) {
            stats = (
                <Fragment>
                    <div style={{ textAlign: 'center' }}>
                        <span className="mdi mdi-check-circle-outline success-icon"></span>
                    </div>
                    <h1>Quiz terminé</h1>
                    <div className="container stats">
                        <h4>{remark}</h4>
                        <h2>Votre Score: {this.state.score.toFixed(0)}&#37;</h2>
                        <span className="stat left">Nombre total de questions: </span>
                        <span className="right">{this.state.numberOfQuestions}</span><br />

                        <span className="stat left">Nombre de questions répondues: </span>
                        <span className="right">{this.state.numberOfAnsweredQuestions}</span><br />

                        <span className="stat left">Nombre de réponses correctes: </span>
                        <span className="right">{this.state.correctAnswers}</span> <br />

                        <span className="stat left">Number de réponses incorrects: </span>
                        <span className="right">{this.state.wrongAnswers}</span><br />

                        <span className="stat left">Indices utilisés: </span>
                        <span className="right">{this.state.hintsUsed}</span><br />

                        <span className="stat left">50-50 utilisés: </span>
                        <span className="right">{this.state.fiftyFiftyUsed}</span>
                    </div>
                    <section>
                        <ul>
                            <li>
                                <Link to ={"/quiz/jeu/"+this.state.niveau}>Rejouer</Link>
                            </li>
                            <li>
                                <Link to ="/quiz">Choisir un niveau</Link>
                            </li>
                        </ul>
                    </section>
                </Fragment>
            );
        } else {
            stats = (
                <section>
                    <h1 className="no-stats">Aucunes statistiques</h1>
                    <ul>
                        <li>
                            <Link to ="/quiz">Sélectionner un niveau</Link>
                        </li>
                    </ul>
                </section>
            );
        }
        return (
            <Fragment>
                <CssBaseline/>
                <div className="quiz-summary">
                    {stats}
                </div>
            </Fragment>
        );
    }
}

export default QuizResultats;