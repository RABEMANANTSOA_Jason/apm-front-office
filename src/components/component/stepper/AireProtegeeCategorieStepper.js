import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import MdArrowRoundBack from 'react-ionicons/lib/MdArrowRoundBack';

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
}));

export default function AireProtegeeCategorieStepper({ categories }) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = categories;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <>
      <Stepper
        activeStep={activeStep}
        orientation="vertical"
        style={{ backgroundColor: "#76ED79" }}
      >
        {categories.map((row) => (
          <Step key={row.id}>
            <StepLabel>{row.statut_fr}</StepLabel>
            <StepContent>
              <Typography>{row.description_categorie_iucn}</Typography>
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Retour
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? "Terminer" : "Suivant"}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper
          square
          elevation={0}
          className={classes.resetContainer}
          style={{ backgroundColor: "#E1FDB0" }}
        >
          <Typography>C'est la fin des catégories</Typography>
          <Link href="/aireprotegee" align="left">
            <MdArrowRoundBack fontSize="60px" color="red" beat={true} />
          </Link>
          <Button
            onClick={handleReset}
            className={classes.button}
            variant="contained"
            color="primary"
          >
            Revoir
          </Button>
          <Button className={classes.button} variant="contained">
            <Link href="/aireprotegee/liste" style={{ color: "green" }}>
              Continuer la visite
            </Link>
          </Button>
        </Paper>
      )}
    </>
  );
}
