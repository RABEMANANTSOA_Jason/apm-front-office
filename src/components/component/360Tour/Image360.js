import React from 'react';
import 'aframe';
export default function image360() {
  return (
    <a-scene>
      <a-sky src={require('https://images.unsplash.com/photo-1557971370-e7298ee473fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1600&q=80')} />
    </a-scene>
  );
}
