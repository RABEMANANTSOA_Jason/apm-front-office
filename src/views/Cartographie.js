import React from 'react';
import L from 'leaflet'
import locate from '../../node_modules/leaflet.locatecontrol';
import CssBaseline from "@material-ui/core/CssBaseline";
import 'leaflet/dist/leaflet.css';
import styled from 'styled-components';
import booleanPointInPolygon from '@turf/boolean-point-in-polygon';
import * as polygon from '@turf/helpers';
import Link from '@material-ui/core/Link';
import API from '../services/API';
import imageURL from '../services/imageURL';
import data from '../components/component/map/data.json';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MdWalk from 'react-ionicons/lib/MdWalk';
const Wrapper = styled.div`
    width: ${props => props.width};
    height: ${props => props.height};
`;
toast.configure()
export default class Cartographie extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            lat : 0,
            lng :0,
        }
    }
    componentDidMount(){
        API.get(`geolocalisation`).then(res => {
        const geojson = res.data;
        this.map = L.map('map',{
            center : [-18.958246,47.516667],
            zoom: 6,
            zoomControl: true,            
        });
        //var geojson = this.state.data;

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            detectRetina : true,
            maxZoom: 20,
            maxNativeZoom: 17,
        }).addTo(this.map);
        
        function customToast(id){
            return(
                <Link href={"/aireprotegee/detail/"+id}>
                    <p style={{color:'white'}}>Vous êtes dans une aire protégée <MdWalk fontSize="30px" color="#black" beat={true} /></p>    
                </Link>
            );
        }
        
        function onEachFeature(feature, layer) {
            // does this feature have a property named popupContent?
            if (feature.properties && feature.properties.NOM_AP_) {
                layer.bindPopup(
                    "<b>"+feature.properties.NOM_AP_
                    +"</b><br/><img src='"+imageURL+feature.properties.image+"' alt='image' width='200px'/>"
                    +"<br/>"
                    +"<p align='center'><a href='/aireprotegee/detail/"+feature.properties.Id+"'>Consulter</a></p>");
            }
        }

        function style(feature) {
            return {
                fillColor: 'green', 
                fillOpacity: 0.5,  
                weight: 2,
                opacity: 1,
                color: '#745210',
                dashArray: '3'
            };
        }

        L.geoJSON(geojson,{
            onEachFeature: onEachFeature,
            style : style
        }).addTo(this.map);
        
        /*var myIcon = L.icon({
            iconUrl: "/icons8-natural-food-48.png",
            iconSize: [38, 95],
            popupAnchor: [-3, -76],
        });*/

        //L.marker([-16.160291687247653,46.10719778174119], {icon: myIcon}).addTo(this.map);

        // create control and add to map
        L.control.locate({
            position: 'bottomright',  // set the location of the control
            drawCircle: true,  // controls whether a circle is drawn that shows the uncertainty about the location
            follow: true,  // follow the user's location
            setView: true, // automatically sets the map view to the user's location, enabled if `follow` is true
            stopFollowingOnDrag: false, // stop following when the map is dragged if `follow` is true (deprecated, see below)
            circleStyle: {},  // change the style of the circle around the user's location
            markerStyle: {},
            showCompass:true,
            followCircleStyle: {},  // set difference for the style of the circle around the user's location while following
            followMarkerStyle: {},
            circlePadding: [0, 0], // padding around accuracy circle, value is passed to setBounds
            metric: true,  // use metric or imperial units
            onLocationError: function(err) {alert(err.message)},  // define an error callback function
            onLocationOutsideMapBounds:  function(context) { // called when outside map boundaries
                    alert(context.options.strings.outsideMapBoundsMsg);
            },
            strings: {
                title: "Me localiser sur la carte",  // title of the locate control
                popup: "<b>Vous êtes ici</b>",  // text to appear if user clicks on circle
                outsideMapBoundsMsg: "Vous vous trouvez en dehors de la carte" // default message for onLocationOutsideMapBounds
                
            },
            locateOptions: {enableHighAccuracy: true, maxZoom: 10}  // define location options e.g enableHighAccuracy: true or maxZoom: 10
        }).addTo(this.map);
        
        function onLocationFound(e){
            var poly;
            var response;
            console.log(response);
            geojson.features.forEach(element => {
                if(element.geometry.type === "Polygon")
                {
                    poly = polygon.polygon(element.geometry.coordinates);
                    response = booleanPointInPolygon([43.77758759012718,-21.375109747332303],poly);
                    if(response){
                        console.log("Polygon : " + element.properties.NOM_AP_);
                        toast.success(customToast(element.properties.Id),{position : toast.POSITION.BOTTOM_CENTER});
                    }
                }
                if(element.geometry.type === "MultiPolygon")
                {
                    poly = polygon.multiPolygon(element.geometry.coordinates);
                    response = booleanPointInPolygon([43.77758759012718,-21.375109747332303],poly);
                    if(response){
                        console.log(element.properties.NOM_AP_);
                        toast.success(customToast(element.properties.Id),{position : toast.POSITION.BOTTOM_CENTER});
                    }                
                }                
            });
        }
        this.map.on('click', function(e) {
            console.log("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng)
        });
        this.map.on('locationfound', onLocationFound);
    });
    }

    render(){
        return(       
            <>  
            <CssBaseline />
            <h3 style={{color:'green'}}>Cartographie des aires protégées de Madagascar</h3>
            <Wrapper width="auto" height="700px" id="map"/>
            </>
        );
    }
}