import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal'
import MdGlasses from 'react-ionicons/lib/MdGlasses';
import VisiteVirtuelle from '../../component/visite_virtuelle/VisiteDetail';
import Link from '@material-ui/core/Link';

export default function VisiteVirtuelleModal({data}){
    const [show, setShow] = useState(false);

    return (
        <>
        <h6 style={{color:'green'}}>Visite virtuelle du site</h6>
        <Link href="#virtual_tour">
            <MdGlasses fontSize="40px" color="black" beat={true} onClick={() => setShow(true)}/>
        </Link>

        <Modal
            show={show}
            onHide={() => setShow(false)}
            dialogClassName="modal-90w"
            aria-labelledby="example-custom-modal-styling-title"
            size="xl"
        >
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
            <VisiteVirtuelle visite={data}/>
            </Modal.Body>
        </Modal>
        </>
    );
}