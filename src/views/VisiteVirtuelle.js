import React, {Component} from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import ListeVisite from '../components/component/visite_virtuelle/ListeVisite';
import API from '../services/API';

class Accueil extends Component{
    constructor (props) {
        super(props);
        this.state = {
            visites: []
        };
    }
    componentDidMount() {
        document.title = "Visites virtuelles";
        API.get(`visite_virtuelle`).then(res => {
            const All = res.data;
            this.setState({ visites:All });
        })
    }

    render(){
        return (
            <React.Fragment>
            <CssBaseline/>
            <h3 style={{color:'green'}}>Visites virtuelles</h3>
            <ListeVisite visites={this.state.visites}/>
            </React.Fragment>
        );
    }
}

export default Accueil;