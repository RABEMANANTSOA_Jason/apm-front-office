import React, {Component} from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import ListeActus from '../components/component/card/ListeActus';
import API from '../services/API';

class Actualites extends Component{
    constructor (props) {
        super(props);
        this.state = {
            actualites: []
        };
    }
    componentDidMount() {
        document.title = "Actualités";
        API.get(`actualites`).then(res => {
            const All = res.data;
            this.setState({ actualites:All });
        })
    }
    render(){
        return(
            <>
            <CssBaseline/>
            <h3 style={{color:'black'}}>Actualités</h3>
            <ListeActus actualites={this.state.actualites}/>
            </>
        )
    }
}

export default Actualites;