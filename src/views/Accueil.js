import React, { Component } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Slide from "../components/component/slide/Slide";
import IosLeafOutline from "react-ionicons/lib/IosLeafOutline";
import Link from "@material-ui/core/Link";
import image from "../assets/images/default.jpg";
class Accueil extends Component {
  componentDidMount() {
    document.title = "Accueil";
  }

  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="md">
          <Slide image={image} />
          <h3 style={{ color: "green" }}>Bienvenue sur le site APM</h3>
          <Typography paragraph align="justify" style={{ color: "black" }}>
            Les aires protégées établies pour sauvegarder la biodiversité et les
            processus écologiques seront probablement influencés de nombreuses
            façons par l’évolution du climat. Ces changements devraient pousser
            les espèces à migrer vers des zones dotées de températures et de
            précipitations plus favorables. Il est très probable que des espèces
            concurrentielles, parfois envahissantes, mieux adaptées au nouveau
            climat s’introduiront. En raison de ces mouvements, des aires
            pourront se retrouver avec des habitats et des espèces autres que
            ceux qu’elles avaient initialement la fonction de sauvegarder. Scott
            (2005), par exemple, a estimé irréaliste l’un des objectifs déclarés
            du parc national Prince Albert au Saskatchewan (Canada), à savoir
            protéger son intégrité écologique «pour l’éternité», car tous les
            scénarios qui existent sur le climat prévoient la perte, un jour,
            des forêts boréales et de leur biodiversité dans cette aire. On
            prévoit que les changements climatiques stimuleront les foyers
            d’infection, les ravageurs devenant plus résistants ou survivant
            plus longtemps, et de nouvelles espèces nuisibles envahissant les
            aires protégées. C’est ainsi qu’il y a près de deux décennies Pounds
            et al. (2006) avaient attribué l’extinction aujourd’hui largement
            proclamée de la grenouille arlequin (Atelopus sp.) et du crapaud
            doré (Bufo periglenes) présents dans la forêt de Monteverde, au
            Costa Rica, au réchauffement des zones tropicales américaines, qui
            aurait, selon les dires, favorisé un champignon particulier qui a
            infecté les amphibiens. Les changements climatiques pourraient
            également accroître l’incidence des incendies dans certaines
            situations et des inondations dans d’autres (GIEC, 2007).
          </Typography>
          <h6 style={{ color: "green" }}>Direction les aires protégées</h6>
          <Link href="/aireprotegee">
            <IosLeafOutline fontSize="60px" color="#43853d" beat={true} />
          </Link>
        </Container>
      </React.Fragment>
    );
  }
}

export default Accueil;
