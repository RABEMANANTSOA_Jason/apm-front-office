import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal'
import MdFlower from 'react-ionicons/lib/MdFlower';
import Image from 'react-bootstrap/Image';
import { Typography } from '@material-ui/core';
import flore from '../../../assets/images/flore.jpg';

export default function FloreModal({icone}){
    const [show, setShow] = useState(false);

    return (
        <>
        <MdFlower fontSize="30px" color="#FF7FEF" rotate={true} onClick={() => setShow(true)}/>

        <Modal show={show} onHide={() => setShow(false)} dialogClassName="modal-90w" 
        aria-labelledby="example-custom-modal-styling-title" size="xl">

            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title">Flore</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Image className="d-block w-100" height="auto" src={flore} alt="" />
            <Typography>

            </Typography>
            </Modal.Body>

        </Modal>
        </>
    );
}