import React, {Component} from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import VisiteDetail from '../../components/component/visite_virtuelle/VisiteDetail';
import API from '../../services/API';

class VisiteVirtuelleDetail extends Component{
    constructor (props) {
        super(props);
        this.state = {
            visite : ''
        };
    }
    componentDidMount() {
        document.title = "Visites virtuelles";
        API.get(`visite_virtuelle/`+this.props.match.params.id).then(res => {
            const All = res.data;
            this.setState({ visite:All });
        })
    }

    render(){
        return (
            <React.Fragment>
            <CssBaseline/>
            <VisiteDetail visite={this.state.visite}/>
            </React.Fragment>
        );
    }
}

export default VisiteVirtuelleDetail;