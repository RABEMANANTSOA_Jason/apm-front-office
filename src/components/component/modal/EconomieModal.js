import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import MdStats from "react-ionicons/lib/MdStats";
import Image from "react-bootstrap/Image";
import { Typography } from "@material-ui/core";
import economie from "../../../assets/images/economie.jpg";

export default function EconomieModal({ icone }) {
  const [show, setShow] = useState(false);

  return (
    <>
      <MdStats
        fontSize="30px"
        color="blue"
        beat={true}
        onClick={() => setShow(true)}
      />

      <Modal
        show={show}
        onHide={() => setShow(false)}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
        size="xl"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Economie
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Image
            className="d-block w-100"
            height="auto"
            src={economie}
            alt=""
          />
          <Typography align="justify">
            Si l’habitat naturel d’un pays est ravagé par les impacts de
            l’altération du climat, son économie en souffrira. Dans une étude
            récente (Dasgupta et al., 2007), les auteurs ont constaté que le
            produit intérieur brut (PIB) d’un certain nombre de pays, le Viet
            Nam en tête, pouvait être négativement influencé par la hausse du
            niveau de la mer, l’intrusion d’eau salée et des catastrophes
            naturelles attribuées aux changements climatiques. En contribuant à
            préserver les habitats naturels, les aires protégées aident à
            soutenir indirectement l’économie nationale. En outre, ces aires
            sont à même de fournir des moyens directs de relever les revenus,
            grâce non seulement au tourisme, mais aussi aux produits de valeur
            qu’elles renferment et aux services qu’elles rendent. C’est ainsi
            que la Réserve de biosphère maya, au Guatemala, assure des emplois à
            plus de 7 000 personnes et produit un revenu annuel d’environ 47
            millions de dollars EU (PCLG, 2002). À Madagascar, d’après une étude
            réalisée sur 41 réserves, le taux de rendement économique du système
            d’aires protégées s’élevait à 54 pour cent et provenait pour une
            large part de la protection des bassins versants et, dans une mesure
            moindre, de l’écotourisme (Naughton-Treves, Buck Holland et Brandon,
            2005). Les aires protégées offrent donc un dispositif de sécurité
            qui peut être précieux en période de stress, par exemple lors
            d’événements climatiques extrêmes. La perte de ces aires pourrait
            entraîner des coûts notables, pour compenser entre autres les
            dommages aux infrastructures et les tragédies humaines causés par la
            désertification ou les raz-de-marée, ou la perte des revenus tirés
            du tourisme. En outre, on a estimé que l’abattage de grands massifs
            forestiers, comme ceux d’Amazonie, a influencé les régimes mondiaux
            de précipitations, nuisant ainsi à l’agriculture et, partant, aux
            moyens d’existence de millions de personnes (Nepstad, 2007). Dès
            lors, les aires protégées non seulement aident à sauvegarder la
            biodiversité, mais contribuent aussi indirectement à la sécurité
            alimentaire de la planète.
          </Typography>
        </Modal.Body>
      </Modal>
    </>
  );
}
