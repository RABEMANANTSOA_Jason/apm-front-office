import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Accueil from '../../views/Accueil'
import AireProtege from '../../views/ap/AireProtege';
import AireProtegeListe from '../../views/ap/AireProtegeListe';
import AireProtegeCategorie from '../../views/ap/AireProtegeCategorie';
import AireProtegeDetail from '../../views/ap/AireProtegeDetail';
import Gestionnaires from '../../views/Gestionnaires';
import Cartographie from '../../views/Cartographie';
import VisiteVirtuelle from '../../views/VisiteVirtuelle';
import VisiteVirtuelleDetail from '../../views/visite/VisiteVirtuelleDetail';
import Quiz from '../../views/quiz/Quiz';
import QuizInstructions from '../../views/quiz/QuizInstructions';
import Jeu from '../../views/quiz/Jeu';
import QuizResultats from '../../views/quiz/QuizResultats';
import Actualites from '../../views/Actualites';
import NotFound from '../../views/NotFound';
export default function Routes() {
  return (
    <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Accueil}/>
          <Route exact path="/index" component={Accueil}/>
          <Route exact path="/accueil" component={Accueil}/>
          <Route exact path="/aireprotegee" component={AireProtege}/>
          <Route exact path="/aireprotegee/categories" component={AireProtegeCategorie}/>
          <Route exact path="/aireprotegee/liste" component={AireProtegeListe}/>
          <Route exact path="/aireprotegee/detail/:id" component={AireProtegeDetail}/>
          <Route exact path="/gestionnaire" component={Gestionnaires}/>
          <Route exact path="/cartographie" component={Cartographie}/>
          <Route exact path="/visite_virtuelle" component={VisiteVirtuelle}/>
          <Route exact path="/visite_virtuelle/detail/:id" component={VisiteVirtuelleDetail}/>
          <Route exact path="/quiz" component={Quiz}/>
          <Route exact path="/quiz/instructions/:id" component={QuizInstructions}/>
          <Route exact path="/quiz/jeu/:id" component={Jeu}/>
          <Route exact path="/quiz/resultats" component={QuizResultats}/>
          <Route exact path="/actualites" component={Actualites}/>
          <Route path="" component={NotFound}/>
        </Switch>
      </BrowserRouter>

  );
}