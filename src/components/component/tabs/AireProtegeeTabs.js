import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MdLeaf from 'react-ionicons/lib/MdLeaf';
import MdWarning from 'react-ionicons/lib/MdWarning';
import MdLocate from 'react-ionicons/lib/MdLocate';
import MdStar from 'react-ionicons/lib/MdStar';
import MdMan from 'react-ionicons/lib/MdMan';
import Container from '@material-ui/core/Container';
import AireprotegeTableGeographiqueDetail from '../tables/AireprotegeTableGeographiqueDetail';
import AireprotegeTableCibleConservation from '../tables/AireprotegeTableCibleConservation';
import PopulationCard from '../card/PopulationCard';
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ScrollableTabsButtonForce({data,cibles,population}) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab icon={<MdLocate fontSize="40px" color="blue" beat={true}/>}/>
          <Tab icon={<MdLeaf fontSize="40px" color="green" beat={true}/>}/>
          <Tab icon={<MdStar fontSize="40px" color="yellow" beat={true}/>}/>
          <Tab icon={<MdMan fontSize="40px" color="blue" beat={true}/>}/>
          <Tab icon={<MdWarning fontSize="40px" color="red" beat={true}/>} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Container>
        <AireprotegeTableGeographiqueDetail data={data}/>
        </Container>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <AireprotegeTableCibleConservation data={cibles}/>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Typography>
        {data.potentialites}
        </Typography>
      </TabPanel>
      <TabPanel value={value} index={3}>
        {population.map((row,index) =>(
          <PopulationCard data={row}/>
        ))}
      </TabPanel>
      <TabPanel value={value} index={4}>
        <Typography align="justify" dangerouslySetInnerHTML={{__html: data.menaces}}/>
      </TabPanel>
    </div>
  );
}
