import React, {Component} from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Recherche from '../../components/component/search/Recherche';
import Link from '@material-ui/core/Link';
import API from '../../services/API';
import MdArrowRoundBack from 'react-ionicons/lib/MdArrowRoundBack';

class Accueil extends Component{
    constructor (props) {
        super(props);
        this.state = {
            regions: [],
            districts: [],
            communes: [],
            fokontany: [],
            aps: [],
            minSuperficie: 0,
            maxSuperficie: 0,
            categories: [],
            gestionnaires: [],
            loading: false,
            limit : 4,
            page: 0,
            prevY: 0
        };
    }
    componentDidMount() {
        document.title = "Les aires protégées";
        API.get(`Region`).then(res => {
            const Allregions = res.data; this.setState({ regions:Allregions });
        });
        API.get(`District`).then(res => {
            const Alldistricts = res.data; this.setState({ districts:Alldistricts });
        });
        API.get(`Aire_protegee/get_min_superficie`).then(res => {
            const Allminsuperficie = parseInt(res.data); this.setState({ minSuperficie:Allminsuperficie });
        });
        API.get(`Aire_protegee/get_max_superficie`).then(res => {
            const Allmaxsuperficie = parseInt(res.data); this.setState({ maxSuperficie:Allmaxsuperficie });
        });
        API.get(`Categorie_iucn`).then(res => {
            const Allcategories = res.data; this.setState({ categories:Allcategories });console.log(this.state.categories);
        });
        API.get(`Gestionnaires`).then(res => {
            const Allgestionnaires = res.data; this.setState({ gestionnaires:Allgestionnaires });console.log(this.state.gestionnaires);
        });
        this.getListeAP(this.state.page);

        var options = {
            root: null,
            rootMargin: "0px",
            threshold: 1.0
          };
          
          this.observer = new IntersectionObserver(
            this.handleObserver.bind(this),
            options
          );
          this.observer.observe(this.loadingRef);

    }

    getListeAP(page) {
        this.setState({ loading: true });
        API.get(`Aire_protegee?page=${page}&limit=${this.state.limit}`)
        .then(res => {
            this.setState({ aps: [...this.state.aps, ...res.data] });
            this.setState({ loading: false });
        });
    }

    handleObserver(entities, observer) {
        const y = entities[0].boundingClientRect.y;
        if (this.state.prevY > y) {
          const curPage = this.state.page + this.state.limit;
          this.getListeAP(curPage);
          this.setState({ page: curPage });
        }
        this.setState({ prevY: y });
    }

    render(){
            // Additional css
        const loadingCSS = {
            height: "100px",
            margin: "30px"
        };
  
        // To change the loading icon behavior
        const loadingTextCSS = { display: this.state.loading ? "block" : "none" };

        return (
            <React.Fragment>
                <CssBaseline />
                <Container maxWidth="md">
                    <h3>Liste des aires protégées</h3>
                    <Recherche regions={this.state.regions} districts={this.state.districts} 
                    aps={this.state.aps} minSup={this.state.minSuperficie} maxSup={this.state.maxSuperficie} categories={this.state.categories} gestionnaires={this.state.gestionnaires}/>
                    <div ref={loadingRef => (this.loadingRef = loadingRef)} style={loadingCSS}>
                    <span style={loadingTextCSS}>Chargement...</span>
                    <Link href="/aireprotegee/categories" align="left">
                        <MdArrowRoundBack fontSize="60px" color="red" beat={true} />
                    </Link>
                    </div>
                </Container>
            </React.Fragment>
        );
    }
}

export default Accueil;