import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import IosContacts from "react-ionicons/lib/IosContacts";
import Image from "react-bootstrap/Image";
import { Typography } from "@material-ui/core";
import social from "../../../assets/images/population.jpg";

export default function SocialModal() {
  const [show, setShow] = useState(false);

  return (
    <>
      <IosContacts
        fontSize="30px"
        color="black"
        beat={true}
        onClick={() => setShow(true)}
      />

      <Modal
        show={show}
        onHide={() => setShow(false)}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
        size="xl"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Social
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Image className="d-block w-100" height="auto" src={social} alt="" />
          <Typography align="justify">
            Les aires protégées pourraient fournir des services propres à
            l’écosystème tels que la disponibilité d’eau potable, le stockage du
            carbone et la stabilisation des sols, contenir des sites considérés
            comme sacrés par certains groupes confessionnels, et renfermer
            d’importants réservoirs de gênes utilisables en médecine,
            agriculture et foresterie. Face aux changements climatiques, les
            rôles que jouent les aires protégées en renforçant les capacités des
            populations locales à s’adapter aux changements climatiques
            acquièrent une nouvelle importance (Simms, 2006). En contribuant au
            maintien des écosystèmes naturels, les aires protégées peuvent
            représenter une protection matérielle contre les grandes
            catastrophes, dont le nombre devrait augmenter avec l’évolution du
            climat (Scheuren et al., 2007). Bien que l’envergure des
            catastrophes dépende normalement d’un ensemble de facteurs (normes
            de construction, utilisation du sol, par exemple), dans de nombreux
            cas le maintien de l’écosystème et la protection de la forêt peuvent
            réduire considérablement leurs impacts. Les mangroves côtières, les
            récifs de corail, les plaines inondables et les forêts peuvent
            servir aux terres, aux communautés et aux infrastructures de zones
            tampons contre les catastrophes naturelles. Pendant le raz-de-marée
            de l’océan Indien en 2004, par exemple, les dunes de sable côtières
            recouvertes de végétation dans les parcs nationaux de Yala et de
            Bundala, à Sri Lanka, ont entièrement stoppé les vagues et protégé
            les terres à l’arrière (Caldecott et Wickremasinghe, 2005).
            Certaines aires protégées encouragent aussi la restauration active
            ou passive des pratiques traditionnelles d’utilisation des terres,
            comme l’agroforesterie et les cultures en terrasses, qui peuvent
            contribuer à atténuer les impacts des événements climatiques
            extrêmes sur les terres arides, par exemple en réduisant le risque
            d’érosion et en maintenant la structure des sols (Stolton, Dudley et
            Randall, 2008). En outre, la gestion des aires protégées permet de
            responsabiliser des populations humaines marginalisées ou certains
            groupes communautaires. De nouvelles formes de gouvernance des aires
            protégées, comme la conservation communautaire ou la cogestion, sont
            mises en œuvre à l’heure actuelle pour réduire les conflits d’ordre
            foncier et promouvoir le maintien à long terme des aires protégées
            au profit des parties prenantes. Un bon exemple de cela est la
            création de «parcs avec les gens», politique formulée en Bolivie en
            2005 pour encourager les communautés autochtones à gérer les aires
            protégées (Peredo-Videa, 2008).
          </Typography>
        </Modal.Body>
      </Modal>
    </>
  );
}
