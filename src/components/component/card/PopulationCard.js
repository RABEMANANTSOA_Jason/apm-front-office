import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import imageURL from '../../../services/imageURL';
import PopulationModal from '../modal/PopulationModal';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },

  cover: {
    width: 151,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}));

export default function PopulationCard({data}) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
            <CardMedia
        className={classes.cover}
        image={imageURL+data.image_population}
        title={data.nom_population}
      />
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Typography component="h5" variant="h5" align="left">
            {data.nom_population}            <PopulationModal titre={data.nom_population} image={data.image_population} description={data.description} impacts={data.impacts}/>
          </Typography>
          <Typography component="h6" variant="h6" align="left">
          </Typography>
        </CardContent>
      </div>
    </Card>
  );
}
