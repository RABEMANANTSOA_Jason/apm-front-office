import React, {Component} from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import VisiteVirtuelleModal from '../../components/component/modal/VisiteVirtuelleModal';

import DetailAP from '../../components/component/ap/DetailAP';
import AireProtegeeTabs from '../../components/component/tabs/AireProtegeeTabs';
import AireProtegeePhotosStepper from '../../components/component/stepper/AireProtegeePhotosStepper';
import LeafletMapDetail from '../../components/component/map/LeafletMapDetail';

import API from '../../services/API';
class AireProtegeDetail extends Component{
    constructor (props) {
        super(props);
        this.state = {
            ap : '',
            cibles : [{}],
            population : [{}],
            visite : '',
            images : [{}]
        };
    }
    componentDidMount() {
        document.title = "";
        API.get(`aire_protegee/`+this.props.match.params.id).then(res => {
            const All = res.data;
            this.setState({ ap:All});
        });
        API.get(`cible_conservation/recherche_cibles_ap/`+this.props.match.params.id).then(res => {
            const All = res.data;
            this.setState({ cibles:All});
            console.log(this.state.cibles);
        });
        API.get(`population/recherche_population_ap/`+this.props.match.params.id).then(res => {
            const All = res.data;
            this.setState({ population:All});
            console.log(this.state.population);
        });
        API.get(`visite_virtuelle/recherche_visite_ap?id=`+this.props.match.params.id).then(res => {
            const All = res.data;
            this.setState({ visite:All});
            console.log(this.state.visite);
        });
        API.get(`images/recherche_gallery_ap?id=`+this.props.match.params.id).then(res => {
            const All = res.data;
            this.setState({ images:All});
            console.log(this.state.images);
        });
    }

    render(){
        return (
            <React.Fragment>
                <CssBaseline />
                <Container maxWidth="lg" style={{backgroundColor:"#E1FDB0"}}>
                    <DetailAP data={this.state.ap}/>
                <Divider/>
                <br/>
                <LeafletMapDetail data={this.props.match.params.id}/>
                <Divider/>
                <br/>
                <Grid container spacing={0}>
                    <Grid item xs={12}>
                        <AireProtegeeTabs data={this.state.ap} cibles={this.state.cibles} population={this.state.population}/>
                    </Grid>
                </Grid>
                <Divider/>
                <br/>
                <VisiteVirtuelleModal data={this.state.visite}/>
                <Grid container spacing={0}>
                    <Grid item xs={12} align="center">
                    <AireProtegeePhotosStepper data={this.state.images}/>
                    </Grid>
                </Grid>
                </Container>
            </React.Fragment>
        );
    }
}

export default AireProtegeDetail;