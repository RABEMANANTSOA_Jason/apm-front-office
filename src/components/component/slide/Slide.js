import React, {useState} from '../../../../node_modules/react';
import Carousel from '../../../../node_modules/react-bootstrap/Carousel';
import Image from 'react-bootstrap/Image';

function renderImages(image){
  return(
    <Carousel.Item>
      <Image
      className="d-block w-100"
      src={image}
      alt="First slide"
      />
    </Carousel.Item>
  );
}

export default function Slide({image}) {
    const [index, setIndex] = useState(0);
  
    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
    };
  
    return (
      <Carousel activeIndex={index} onSelect={handleSelect}>
        {renderImages(image)}
        {renderImages(image)}
        {renderImages(image)}
      </Carousel>
    );
  }
