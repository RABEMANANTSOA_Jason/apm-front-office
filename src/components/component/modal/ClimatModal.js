import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal'
import MdThermometer from 'react-ionicons/lib/MdThermometer';
import Image from 'react-bootstrap/Image';
import { Typography } from '@material-ui/core';
import climat from '../../../assets/images/climat.jpg';
export default function ClimatModal(){
    const [show, setShow] = useState(false);

    return (
        <>
        <MdThermometer fontSize="30px" color="red" beat={true} onClick={() => setShow(true)}/>

        <Modal show={show} onHide={() => setShow(false)} dialogClassName="modal-90w" 
        aria-labelledby="example-custom-modal-styling-title" size="xl">

            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title">Climat</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Image className="d-block w-100" height="auto" src={climat} alt="" />
            <Typography>
            Pendant plusieurs décennies, les aires protégées ont été considérées comme un outil essentiel pour la conservation de la biodiversité. Les impacts des changements climatiques leur attribuent maintenant un rôle renouvelé comme outils d’adaptation à l’altération du climat. Elles ont une triple fonction à remplir à cet égard:

aider les espèces à s’adapter aux schémas de changements climatiques et aux événements climatiques soudains, en fournissant des refuges et des couloirs de migration;
protéger les populations contre les événements climatiques soudains et réduire la vulnérabilité aux inondations, aux sécheresses et à d’autres catastrophes liées au climat;
aider indirectement les économies à s’adapter aux changements climatiques par la réduction des coûts découlant des effets préjudiciables d’événements climatiques.
            </Typography>
            </Modal.Body>

        </Modal>
        </>
    );
}