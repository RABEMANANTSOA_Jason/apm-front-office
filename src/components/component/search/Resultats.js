import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import imageURL from '../../../services/imageURL';
import IosWalk from 'react-ionicons/lib/IosWalk';

const useStyles = makeStyles({
    root: {
      maxWidth: 345,
    }
  });

export default function Resultats({aps_all,recherche, aps_result}) {
    const liste_ap = aps_all;
    const liste_result_search = aps_result;
  return (
    <React.Fragment>
        
    {recherche? (
        isresultat(liste_result_search)? (
            <Grid container spacing={3}>
            {liste_result_search.map((row) => (
                <Grid item xs={6} key={row.id_ap}>
                    {card(row.id_ap,row.nom_du_site,row.ap_image)}
                </Grid>
            ))}
        </Grid>
        ) : (
            <h3>Aucun résultats</h3>
        )
    ) : (
        isresultat(liste_ap)? (
            <Grid container spacing={3}>
            {liste_ap.map((row) => (
                <Grid item xs={6} key={row.id_ap}>
                    {card(row.id_ap,row.nom_du_site,row.ap_image)}
                </Grid>
            ))}
        </Grid>
        ) : (
            <h3>Aucun résultats</h3>
        )
    )}
       
    </React.Fragment>
  );
}

function isresultat(aps){
    if(aps.length > 0){ return true;}
    return false;
}

function card(id,nom_ap,image){
    console.log(imageURL+image);
    return(
        <Card className={useStyles.root}>
        <CardActionArea>
            <CardMedia
            component="img"
            alt=""
            height="140"
            image={imageURL+image}
            title={nom_ap}
            />
            <CardContent>
                <Typography gutterBottom variant="h6" component="h4">
                    {nom_ap}
                </Typography>
            </CardContent>
        </CardActionArea>
        <CardActions>
            <Link href={"detail/"+id}>
                <p><IosWalk fontSize="60px" color="blue" beat={true} />Visiter</p>
            </Link>
        </CardActions>
        </Card>
    );
}