/* eslint-disable no-use-before-define */
import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import Resultats from './Resultats';
import IosSearch from 'react-ionicons/lib/IosSearch';
import MdFunnel from 'react-ionicons/lib/MdFunnel';
import API from '../../../services/API';
import Slider from '@material-ui/core/Slider';
import Tooltip from '@material-ui/core/Tooltip';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  root1: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 'auto',
  },
  card: {
    flexGrow: 1,
    backgroundColor : '#fff' 
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

function ValueLabelComponent(props) {
  const { children, open, value } = props;

  return (
    <Tooltip open={open} enterTouchDelay={0} placement="top" title={value}>
      {children}
    </Tooltip>
  );
}

ValueLabelComponent.propTypes = {
  children: PropTypes.element.isRequired,
  open: PropTypes.bool.isRequired,
  value: PropTypes.number.isRequired,
};

export default function Recherche({regions,districts,communes,fokontany,minSup,maxSup,categories,gestionnaires,aps}) {
  const classes = useStyles();
  const [mot, Setmot] = useState('');
  const [region, Setregion] = useState('');
  const [district, Setdistrict] = useState('');
  const [liste_ap, Setliste_ap] = useState([]);
  const [value, setValue] = useState([minSup, maxSup]);
  const [categorie, Setcategorie] = useState(-1);
  const [gestionnaire, Setgestionnaire] = useState(-1);

  const [isrecherche, setIsrecherche] = useState(false);
  const [firstRecherche, setFirstRecherche] = useState('');

  const handleChange = (event, newValue) => {
    setValue(newValue);
    console.log(value);
  };

  const optionsRegions = regions.map((option) => {
    const firstLetter = option.region[0].toUpperCase();
    return {
      firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
      ...option,
    };
  });

  const optionsDistricts = districts.map((option) => {
    const firstLetter = option.district[0].toUpperCase();
    return {
      firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
      ...option,
    };
  });

  const optionsCategories = categories.map((option) => {
    const firstLetter = option.statut_fr[0].toUpperCase();
    return {
      firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
      ...option,
    };
  });

  const optionsGestionnaires = gestionnaires.map((option) => {
      const firstLetter = option.nom_gestionnaire[0].toUpperCase();
      return {
        firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
        ...option,
      };
    });

  const recherche = evt =>{
    if(evt.key === "Enter"){
      setFirstRecherche(`?mot=`.mot);
      API.get(`Aire_protegee/rechercheglobal?mot=`+mot).then(res => {
        const Allaireprotege = res.data; Setliste_ap(Allaireprotege );console.log(liste_ap);
      });
      setIsrecherche(true);
    }
  }

  const rechercheClick = () => {
    setFirstRecherche(`?mot=`.mot);
    API.get(`Aire_protegee/rechercheglobal?mot=`+mot).then(res => {
      const Allaireprotege = res.data; Setliste_ap(Allaireprotege );console.log(liste_ap);
    });
    setIsrecherche(true);
  }

  const handleChangeRegion = (event, newValue) => {
    if(newValue != null){
      Setregion(newValue.region);
      setFirstRecherche(`?region=`+newValue.region);
      API.get(`Aire_protegee/rechercheglobal?region=`+newValue.region).then(res => {
        const Allaireprotege = res.data; Setliste_ap(Allaireprotege );console.log(liste_ap);
      });
      setIsrecherche(true);
    }
  };
  const handleChangeDistrict = (event, newValue) => {
    if(newValue != null){
      Setregion(newValue.district);
      setFirstRecherche(`?district=`+newValue.district);
      API.get(`Aire_protegee/rechercheglobal?district=`+newValue.district).then(res => {
        const Allaireprotege = res.data; Setliste_ap(Allaireprotege );console.log(liste_ap);
      });
    }
  };
  const handleChangeCommunes = (event, newValue) => {
    if(newValue != null){
      Setregion(newValue.region);
      setFirstRecherche(`?commune=`+newValue.commune);
      API.get(`Aire_protegee/rechercheglobal?commune=`+newValue.commune).then(res => {
        const Allaireprotege = res.data; Setliste_ap(Allaireprotege );console.log(liste_ap);
      });
    }
  };
  const handleChangeFokontany = (event, newValue) => {
    if(newValue != null){
      Setregion(newValue.region);
      setFirstRecherche(`?fokontany=`+newValue.fokontany);
      API.get(`Aire_protegee/rechercheglobal?fokontany=`+newValue.fokontany).then(res => {
        const Allaireprotege = res.data; Setliste_ap(Allaireprotege );console.log(liste_ap);
      });
    }
  };
  const handleChangeCategorie = (event, newValue) => {
    if(newValue != null){
      Setcategorie(newValue.id);
    }
    else{
      Setcategorie(-1);
    }
  };
  const handleChangeGestionnaire = (event, newValue) => {
    if(newValue != null){
      Setgestionnaire(newValue.id);
    }
    else{
      Setgestionnaire(-1);
    }
  };

  const handleChangeTri = () => {
      API.get(`Aire_protegee/tri?min=`+value[0]+`&max=`+value[1]+`&categorie=`+categorie+`&gestionnaire=`+gestionnaire).then(res => {
        const Allaireprotege = res.data; Setliste_ap(Allaireprotege );console.log(liste_ap);
      });
      setIsrecherche(true);
  };
  
  return (
    <>
      <div className={classes.root}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
            <Paper className={classes.root1}>
              <InputBase
                className={classes.input}
                placeholder="Recherhe par mot clé"
                inputProps={{ 'aria-label': 'Recherhe par mot clé' }}
                onChange={e => Setmot(e.target.value)}
                value={mot}
                onKeyPress={recherche}
              />
              <IconButton className={classes.iconButton} aria-label="search" onClick={rechercheClick}>
                <IosSearch fontSize="30px" color="#43853d" beat={true}/>
              </IconButton>
            </Paper>
            </Grid>
            <Grid item xs={6}>
              <Autocomplete
                id="grouped-demo"
                options={optionsRegions.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                groupBy={(option) => option.firstLetter}
                size="small"
                getOptionLabel={(option) => option.region}
                renderInput={(params) => <TextField {...params} label="Régions" variant="outlined" />}
                getOptionSelected={(option, value) => option.region === value.region}
                onChange={handleChangeRegion}
              />
            </Grid>
            <Grid item xs={6}>
            <Autocomplete
                id="grouped-demo"
                options={optionsDistricts.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                groupBy={(option) => option.firstLetter}
                size="small"
                getOptionLabel={(option) => option.district}
                renderInput={(params) => <TextField {...params} label="Districts" variant="outlined" />}
                getOptionSelected={(option, value) => option.district === value.district}
                onChange={handleChangeDistrict}
              />
            </Grid>
            <Grid item xs={6}>
            <Autocomplete
                id="grouped-demo"
                options={optionsRegions.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                groupBy={(option) => option.firstLetter}
                size="small"
                getOptionLabel={(option) => option.region}
                renderInput={(params) => <TextField {...params} label="Communes" variant="outlined" />}
              />
            </Grid>
            <Grid item xs={6}>
            <Autocomplete
                id="grouped-demo"
                options={optionsRegions.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                groupBy={(option) => option.firstLetter}
                size="small"
                getOptionLabel={(option) => option.region}
                renderInput={(params) => <TextField {...params} label="Fokontany" variant="outlined" />}
              />
            </Grid>
            <Grid item xs={10}>
                <Typography id="range-slider" gutterBottom align="left">
                  Superficie
                </Typography>
                <Slider
                  value={value}
                  onChange={handleChange}
                  ValueLabelComponent={ValueLabelComponent}
                  aria-label="custom thumb label"
                  min={minSup}
                  max={maxSup}
                />
            </Grid>
            <Grid item xs={2}>
            <Typography id="range-slider" gutterBottom align="center">
                  Trier
                </Typography>
            <MdFunnel fontSize="30px" color="#43853d" beat={true} onClick={handleChangeTri}/>
            </Grid>
            <Grid item xs={6}>
            <Autocomplete
                id="grouped-demo"
                options={optionsCategories.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                groupBy={(option) => option.firstLetter}
                size="small"
                getOptionLabel={(option) => option.statut_fr}
                renderInput={(params) => <TextField {...params} label="Catégoies" variant="outlined" />}
                getOptionSelected={(option, value) => option.id === value.id}
                onChange={handleChangeCategorie}
              />
            </Grid>
            <Grid item xs={6}>
            <Autocomplete
                id="grouped-demo"
                options={optionsGestionnaires.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                groupBy={(option) => option.firstLetter}
                size="small"
                getOptionLabel={(option) => option.nom_gestionnaire}
                renderInput={(params) => <TextField {...params} label="Gestionanires" variant="outlined" />}
                getOptionSelected={(option, value) => option.id === value.id}
                onChange={handleChangeGestionnaire}
              />
            </Grid>
          </Grid>
        <br/>
        <Resultats aps_all={aps} recherche={isrecherche} aps_result={liste_ap}/>
      </div>
    </>
  );
}

