import axios from 'axios';

export default axios.create({
  //baseURL: `http://apm.localhost/`
  baseURL: `https://apm-server-rest.herokuapp.com/`
});