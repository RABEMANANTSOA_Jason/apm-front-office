import React,{useEffect} from 'react';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button } from '@material-ui/core';

toast.configure()
export default function ToastCustom({message}){
  const notify = () =>{
    toast.error(message)
  }

  useEffect(() =>{
    notify();
  },[]);

  return (
    <></>
  )
}